<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(\route('platform.index'));
});

// Word requests
Route::group(['as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['platform',]], function () {
    Route::resource('word-request', \App\Http\Controllers\Admin\WordRequestController::class)
        ->middleware(\App\Http\Middleware\Admin\HasWordRequestAccess::class);

    Route::resource('questions', \App\Http\Controllers\Admin\QuestionsController::class)
        ->middleware(\App\Http\Middleware\Admin\HasQuestionsAccess::class);

    Route::resource('corrections', \App\Http\Controllers\Admin\CorrectionController::class)
        ->middleware(\App\Http\Middleware\Admin\HasCorrectionsAccess::class);

    Route::resource('wr', \App\Http\Controllers\Admin\WRController::class)
        ->middleware(\App\Http\Middleware\Admin\HasWordRequestAccess::class);
});
