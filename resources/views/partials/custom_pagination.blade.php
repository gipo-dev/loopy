@if ($paginator->hasPages())
    @php
        if(isset($elements[3]))
            unset($elements[3]);
        if(isset($elements[4]))
            unset($elements[4]);
    @endphp
    <ul class="pagination">

        @if ($paginator->onFirstPage())
            <li class="page-item  disabled"><span class="page-link">←</span></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">←</a></li>
        @endif



        @foreach ($elements as $element)

            @if (is_string($element))
                <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
            @endif



            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active my-active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class="page-item"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach



        @if ($paginator->hasMorePages())
            <li class="page-item"><a href="{{ $paginator->nextPageUrl() }}" class="page-link" rel="next">→</a></li>
        @else
            <li class="page-item disabled"><span class="page-link">→</span></li>
        @endif
    </ul>
@endif
