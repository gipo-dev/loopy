<div>
    @php $trusted = Auth::user()->hasAccess('word-requests.trusted'); @endphp
    <div class="px-2">
        <div class="float-right row">
            <div class="col">
                <button class="btn btn-outline-danger" id="deleteChecked">
                    Удалить выделенные
                </button>
            </div>

            <div class="col d-flex justify-content-end">
                <button wire:click="publish()" class="btn btn-outline-success"
                        {{ Auth::user()->hasAccess('corrections.trusted') ? '' : 'disabled' }}
                        wire:loading.class="disabled">
                    Опубликовать проверенные ({{ $wait_for_publish }})
                </button>
            </div>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th width="50">
                <input type="checkbox" id="checkAll">
            </th>
            <th scope="col">
                @if($sorting['row'] == '')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('')">#</a>
            </th>
            @if($trusted)
                <th>Статус</th>
            @endif
            <th scope="col">
                @if($sorting['row'] == 'word')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('word')">Слово</a>
            </th>
            <th scope="col">
                @if($sorting['row'] == 'meaning')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('meaning')">Определение</a>
            </th>
            <th scope="col">
                @if($sorting['row'] == 'time')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('time')">Дата</a>
                <button type="button" class="btn btn-primary btn-sm ml-1" id="period-picker">Период</button>
                <input type="hidden" id="period_from" wire:model="period.from">
                <input type="hidden" id="period_to" wire:model="period.to">
            </th>
            <th scope="col">
                @if($sorting['row'] == 'count')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('count')">Вес</a>
            </th>
            <th scope="col">#</th>
        </tr>
        </thead>
        <tbody>
        @foreach($word_requests as $word_request)
            <tr class="@if(isset($busy_posts[$word_request->id])) disabled @endif">
                <td>
                    <input type="checkbox" value="{{ $word_request->id }}">
                </td>
                <th scope="row">{{ $word_request->id }}</th>
                @if($trusted)
                    <td>{{ __('admin.status.' . $word_request->status_id) }} <small
                            class="text-secondary">{{ $word_request->editor->email ?? '' }}</small></td>
                @endif
                <td>
                    {{ $word_request->word ?? '' }}
                </td>
                <td>
                    {{ $word_request->meaning ?? '' }}
                </td>
                <td>
                    @if($word_request->time)
                        {{ $word_request->time->format('H:i d.m.Y') }}
                    @endif
                </td>
                <td>
                    {{ $word_request->count ?? '' }}
                </td>
                <td>
                    <button class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modal_edit"
                            wire:click="edit({{ $word_request }}, 'meaning')">
                        Ред.
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $word_requests->links('partials.custom_pagination') }}
    <div wire:ignore.self class="modal fade" id="modal_edit" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Определение</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="save">
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Слово</label>
                            <input type="text" wire:model="editable_post.word" class="form-control" required
                                   minlength="2">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Определение</label>
                            <input type="text" wire:model="editable_post.meaning" class="form-control" required
                                   minlength="2">
                        </div>
                        <div class="mb-3">
                            <a href="https://www.google.com/search?q={{ urlencode($editable_post['meaning'] ?? '') }}"
                               target="_blank" class="btn btn-link">
                                Google
                            </a>
                            <a href="https://yandex.ru/search/?lr=2&text={{ urlencode('define:' . ($editable_post['meaning'] ?? '')) }}"
                               target="_blank" class="btn btn-link">
                                Yandex
                            </a>
                            <a href="https://ru.wikipedia.org/wiki/{{ urlencode($editable_post['meaning'] ?? '') }}"
                               target="_blank" class="btn btn-link">
                                Wikipedia
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="float-left btn btn-danger" style="margin-right: auto;"
                        wire:click="delete({{ $editable_post['id'] ?? null }})">Удалить</span>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.21.1/daterangepicker.min.css" rel="stylesheet"/>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.21.1/jquery.daterangepicker.min.js"></script>
<script>
    var dtp = $('#period-picker').dateRangePicker({});

    document.addEventListener('livewire:load', function () {
        dtp.bind('datepicker-change', function (event, obj) {
        @this.set('period.from', obj.date1);
        @this.set('period.to', obj.date2);
        });

        $('#checkAll').change(function () {
            $('tr td [type=checkbox]').attr('checked', $(this).is(':checked'));
        });

        $('#deleteChecked').click(function (e) {
            e.preventDefault();
            if (confirm('Подтверждаете удаление выделенных элементов?')) {
                var ids = [];
                $('tr td [type=checkbox]:checked').each(function (i, el) {
                    ids.push($(el).val());
                })
            @this.delete(ids);
            }
        });
    });
</script>
