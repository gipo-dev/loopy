<div wire:ignore.self class="modal fade" id="modal_edit_type_1" tabindex="-1" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Добавление</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent="save">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Слово</label>
                        <input type="text" wire:model="editable_post.word" class="form-control" required style="font-weight: bold;"
                               minlength="2">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Источник</label>
                        <input type="text" wire:model="editable_post.source" class="form-control"
                               minlength="2">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">
                            <strong>Определения</strong>
                        </label>
                        @if($editable_post['meanings'] ?? false)
                            @foreach($editable_post['meanings'] as $i => $meaning)
                                <div class="mb-2">
                                    <input type="text" wire:model="editable_post.meanings.{{ $i }}" class="form-control" required
                                           minlength="2">
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="mb-3">
                        <a href="https://www.google.com/search?q={{ urlencode($editable_post['word'] ?? '') }}"
                           target="_blank" class="btn btn-link">
                            Google
                        </a>
                        <a href="https://ru.wikipedia.org/wiki/{{ urlencode($editable_post['word'] ?? '') }}"
                           target="_blank" class="btn btn-link">
                            Wikipedia
                        </a>
                        <a href="https://www.google.com/search?q={{ urlencode('define:' . ($editable_post['word'] ?? '')) }}"
                           target="_blank" class="btn btn-link">
                            Определение
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                        <span class="float-left btn btn-danger" style="margin-right: auto;"
                              wire:click="delete({{ $editable_post['id'] ?? null }})">Удалить</span>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>
