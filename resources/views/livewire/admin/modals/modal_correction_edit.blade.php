<div wire:ignore.self class="modal fade" id="modal_edit_type_0" tabindex="-1" aria-labelledby="exampleModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Модерация правки</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent="save">
                <div class="modal-body">
                    <div class="row">
                        <div class="col mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Слово</label>
                            <input type="text" wire:model="editable_post.word" class="form-control" required
                                   style="font-weight: bold;"
                                   minlength="2">
                        </div>
                        <div class="col mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Источник</label>
                            <input type="text" wire:model="editable_post.source" class="form-control"
                                   minlength="2">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">
                            <strong>Определения</strong>
                        </label>
                        @isset($editable_post['word_id'])
                            @php $word_meanings = (\App\Models\Word::find($editable_post['word_id'])->meanings ?? collect([]))->keyBy('id') @endphp
                        @endisset
                        @if($editable_post['meanings'] ?? false)
                            @foreach(array_reverse($editable_post['meanings'], true) as $i => $meaning)
                                <div class="mb-2">
                                    @if($i == 0)
                                        <label>
                                            Новое определение
                                        </label>
                                    @endif
                                    @if($i != 0)
                                        <textarea type="text" required wire:model.lazy="editable_post.meanings.{{ $i }}"
                                                  style="height: 1px;" rows="1"
                                                  class="mb-1 form-control bg- @isset($word_meanings[$i]) {{ trim($word_meanings[$i]->meaning) == trim($meaning) ? 'bg-success text-white' : 'bg-warning' }} @endif"></textarea>
                                    @else
                                        <textarea type="text" required wire:model.lazy="editable_post.meanings.{{ $i }}"
                                                  class="mb-1 form-control" rows="1"></textarea>
                                    @endif
                                    {{--                                    <input type="text" class="form-control"--}}
                                    {{--                                           required--}}
                                    {{--                                           minlength="2">--}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="mb-3">
                        <a href="https://www.google.com/search?q={{ urlencode($editable_post['word'] ?? '') }}"
                           target="_blank" class="btn btn-link">
                            Google
                        </a>
                        <a href="https://ru.wikipedia.org/wiki/{{ urlencode($editable_post['word'] ?? '') }}"
                           target="_blank" class="btn btn-link">
                            Wikipedia
                        </a>
                        <a href="https://www.google.com/search?q={{ urlencode('define:' . ($editable_post['word'] ?? '')) }}"
                           target="_blank" class="btn btn-link">
                            Определение
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                        <span class="float-left btn btn-danger" style="margin-right: auto;"
                              wire:click="delete({{ $editable_post['id'] ?? null }})">Удалить</span>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>
