<div>
    <table class="table">
        <thead>
        <th>Слово</th>
        <th>Значения</th>
        <th>#</th>
        </thead>
        <tbody>
        @foreach($words as $word)
            <tr class="bg-www">
                <td>{{ $word->word }}</td>
                <td><a href='https://www.google.com/search?q="{{ $word->word }}"' target="_blank">google</a></td>
                <td>
{{--                    <button class="btn btn-light btn-sm" onclick="confirm('Удалить?') || event.stopImmediatePropagation()" wire:click="deleteWord({{ $word->id }})">--}}
{{--                        Удалить слово--}}
{{--                    </button>--}}
                </td>
            </tr>
            @foreach($word->meanings_new as $meaning)
                <tr>
                    <td></td>
                    <td>{{ $meaning->meaning }}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Удалить?') || event.stopImmediatePropagation()" wire:click="deleteDef({{ $meaning->id }})">Удалить определение</button>
                    </td>
                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
    {{ $words->onEachSide(1)->links() }}
</div>
