<div>
    @php $trusted = Auth::user()->hasAccess('questions.trusted'); @endphp
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя</th>
            <th scope="col">E-mail</th>
            <th scope="col">Вопрос</th>
            <th scope="col">Создан</th>
            @if($trusted)
                <th>Статус</th>
            @endif
            <th scope="col">#</th>
        </tr>
        </thead>
        <tbody>
        @foreach($questions as $question)
            <tr>
                <th scope="row">{{ $question->id }}</th>
                <td>
                    {{ $question->name ?? '' }}
                </td>
                <td>
                    {{ $question->email ?? '' }}
                </td>
                <td>
                    {{ $question->question ?? '' }}
                </td>
                <td>
                    @if($question->date)
                        {{ $question->date->diffForHumans() }}
                    @endif
                </td>
                @if($trusted)
                    <td>{{ __('admin.questions.status.' . $question->status) }}</td>
                @endif
                <td>
                    <button class="btn btn-outline-primary btn-sm" wire:click="publish({{ $question }})">
                        Опубликовать
                    </button>
                    <button class="btn btn-outline-danger btn-sm"
                            onclick="confirm('Подтверждаете удаление?') || event.stopImmediatePropagation()"
                            wire:click="delete({{ $question }})">
                        Удалить
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $questions->onEachSide(1)->links() }}
</div>
