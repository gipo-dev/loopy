<div>
    @php $trusted = Auth::user()->hasAccess('corrections.trusted'); @endphp
    <div class="px-2">
        <div class="float-right row">
            <div class="col">
                <button class="btn btn-outline-danger" id="deleteChecked">
                    Удалить выделенные
                </button>
            </div>
            <div class="col-md-2">
                <input type="text" class="form-control" wire:model="search" placeholder="Поисковое слово...">
            </div>
            <div class="col-md-2">
                <select wire:model="search_field" class="form-control">
                    <option value="email">E-mail</option>
                    <option value="id">ID</option>
                    <option value="word">Слово</option>
                </select>
            </div>
            <div class="col">
                <a href="?word={{ $search }}&field={{ $search_field }}" class="btn btn-outline-primary">Применить</a>
            </div>
            <div class="col">
                <button wire:click="publish()" class="btn btn-outline-success"
                        {{ Auth::user()->hasAccess('corrections.trusted') ? '' : 'disabled' }}
                        wire:loading.class="disabled">
                    Опубликовать проверенные ({{ $wait_for_publish }})
                </button>
            </div>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th width="50">
                <input type="checkbox" id="checkAll">
            </th>
            <th scope="col">
                @if($sorting['row'] == '')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('id')">#</a>
            </th>
            @if($trusted)
                <th>Статус</th>
            @endif
            <th scope="col">
                @if($sorting['row'] == 'type')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('type')">Тип</a>
            </th>
            <th scope="col">
                @if($sorting['row'] == 'word')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('word')">Слово</a>
            </th>
            <th scope="col">
                @if($sorting['row'] == 'date')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('date')">Дата</a>
                <button type="button" class="btn btn-primary btn-sm ml-1" id="period-picker">Период</button>
                <input type="hidden" id="period_from" wire:model="period.from">
                <input type="hidden" id="period_to" wire:model="period.to">
            </th>
            <th scope="col">
                @if($sorting['row'] == 'counter')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('counter')">Вес</a>
            </th>
            <th scope="col">
                @if($sorting['row'] == 'email')
                    @if($sorting['desc'])▲@elseif($sorting['desc'] == false)▼@endif
                @endif
                <a href="" wire:click.prevent="sort('email')">Email</a>
            </th>
            <th scope="col">#</th>
        </tr>
        </thead>
        <tbody>
        @foreach($corrections as $correction)
            @if($correction->type == \App\Models\Correction::TYPE_EDIT && !isset($correction->meanings[0]))
                @continue
            @endif
            <tr class="@if(isset($busy_posts[$correction->id])) disabled @endif">
                <td>
                    <input type="checkbox" value="{{ $correction->id }}">
                </td>
                <th scope="row">{{ $correction->id }}</th>
                @if($trusted)
                    <td>{{ __('admin.corrections.status.' . $correction->status) }} <small
                            class="text-secondary">{{ $correction->editor->email ?? '' }}</small></td>
                @endif
                <td>
                    {{ $correction->typeName ?? '' }}
                </td>
                <td>
                    <a href="#" data-bs-toggle="modal"
                       data-bs-target="#modal_edit_type_{{ $correction->type }}"
                       wire:click.prevent="edit({{ $correction }}, 'meaning')">{{ $correction->word ?? '' }}</a>
                </td>
                <td>
                    @if($correction->date)
                        {{ $correction->date->format('H:i d.m.Y') }}
                    @endif
                </td>
                <td>
                    {{ $correction->counter ?? '' }}
                </td>
                <td>
                    {{ $correction->email ?? '' }}
                </td>
                <td>
                    <button class="btn btn-outline-primary btn-sm" data-bs-toggle="modal"
                            data-bs-target="#modal_edit_type_{{ $correction->type }}"
                            wire:click="edit({{ $correction }}, 'meaning')">
                        Модерировать
                    </button>
                    <button class="btn btn-outline-danger btn-sm"
                            wire:click="delete({{ $correction->id }})">
                        Удалить
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $corrections->onEachSide(3)->links() }}

    @include('livewire.admin.modals.modal_correction_new')
    @include('livewire.admin.modals.modal_correction_edit')
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.21.1/daterangepicker.min.css"
      rel="stylesheet"/>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.21.1/jquery.daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/autosize@5.0.0/dist/autosize.min.js"></script>
<script>
    var dtp = $('#period-picker').dateRangePicker({});

    document.addEventListener('livewire:load', function () {
        dtp.bind('datepicker-change', function (event, obj) {
            console.log(obj.date1)
        @this.set('period.from', obj.date1);
        @this.set('period.to', obj.date2);
            location.href = encodeURI('{{ request()->url() }}?period_from=' + obj.date1.toLocaleDateString() + '&period_to=' + obj.date2.toLocaleDateString());
        });

        $('#checkAll').change(function () {
            $('tr td [type=checkbox]').attr('checked', $(this).is(':checked'));
        });

        $('#deleteChecked').click(function (e) {
            e.preventDefault();
            if (confirm('Подтверждаете удаление выделенных элементов?')) {
                var ids = [];
                $('tr td [type=checkbox]:checked').each(function (i, el) {
                    ids.push($(el).val());
                })
            @this.delete(ids);
            }
        });

        Livewire.hook('message.received', (message, component) => {
            setTimeout(function () {
                var el = $('#modal_edit_type_0 textarea');
                autosize(el);
                autosize.update(el);
            }, 50);
        })
    });
</script>
