@extends('admin.body')

@section('title')Слова@endsection

@section('content')
    @livewire('admin.word-requests-table')
@endsection

@push('styles')
    <style>
        tr.disabled {
            pointer-events: none;
            opacity: .3;
        }
    </style>
@endpush
@push('scripts')
    <script>
        try {
            window.livewire.on('modelSaved', () => {
                var myModalEl = document.getElementById('modal_edit');
                var modal = bootstrap.Modal.getInstance(myModalEl);
                modal.hide();
            });
        } catch (e) {
            location.reload();
        }
    </script>
@endpush
