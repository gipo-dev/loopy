@extends('admin.body')

@section('title')Правки и добавления@endsection

@section('content')
    @livewire('admin.corrections-table')
@endsection

@push('styles')
    <style>
        tr.disabled {
            pointer-events: none;
            opacity: .3;
        }
    </style>
@endpush
@push('scripts')
    <script>
        try {
            window.livewire.on('modelSaved', () => {
                $('.btn-close').trigger('click');
                var myModalEl = document.querySelectorAll('#modal_edit_type_0, #modal_edit_type_1');
                myModalEl.forEach(function (el, i) {
                    var modal = bootstrap.Modal.getInstance(el);
                    modal.hide();
                })
            });
        } catch (e) {
            location.reload();
        }
    </script>
@endpush
