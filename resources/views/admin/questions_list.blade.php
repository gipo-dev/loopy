@extends('admin.body')

@section('title')Управление отзывами@endsection

@section('content')
    @livewire('admin.questions-table')
@endsection

@push('styles')
    <style>
        tr.disabled {
            pointer-events: none;
            opacity: .3;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript">
        try {
            window.livewire.on('modelSaved', () => {
                var myModalEl = document.getElementById('modal_edit');
                var modal = bootstrap.Modal.getInstance(myModalEl);
                modal.hide();
            });
        } catch (e) {
            location.reload();
        }
    </script>
@endpush
