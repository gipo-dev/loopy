<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Панель администратора')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    @livewireStyles
    @stack('styles')
</head>
<body>
<div class="d-flex justify-content-between">
    @if(Auth::user()->hasAccess('platform.systems.users'))
        <a href="{{ route('platform.main') }}" class="btn btn-link">
            Админ-панель
        </a>
    @endif
    @if(Auth::user()->hasAccess('word-requests.edit'))
        <a href="{{ route('admin.word-request.index') }}" class="btn btn-link">
            Управление словами
        </a>
    @endif
    @if(Auth::user()->hasAccess('corrections.edit'))
        <a href="{{ route('admin.corrections.index') }}" class="btn btn-link">
            Правки и добавления
        </a>
    @endif
    @if(Auth::user()->hasAccess('questions.edit'))
        <a href="{{ route('admin.questions.index') }}" class="btn btn-link">
            Управление отзывами
        </a>
    @endif
    <form id="logout-form" action="{{ route('platform.logout') }}" method="POST">
        {{ csrf_field() }}
        <button class="btn btn-link">Выход</button>
    </form>
</div>

@yield('content')

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ mix('/js/app.js') }}"></script>
@livewireScripts
@stack('scripts')
</body>
</html>
