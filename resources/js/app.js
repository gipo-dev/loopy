require('./bootstrap');

var lastEvent = 0;

window.addEventListener('alert', event => {
    if (lastEvent == event.timeStamp)
        return;
    toastr[event.detail.type](event.detail.message,
        event.detail.title ?? ''), toastr.options = {
        "closeButton": true,
        "progressBar": true,
    }
    lastEvent = event.timeStamp;
});
