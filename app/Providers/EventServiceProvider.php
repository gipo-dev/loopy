<?php

namespace App\Providers;

use App\Events\Events\AddToStatistic;
use App\Events\Events\Admin\CorrectionSaved;
use App\Events\Events\Admin\WordRequestSaved;
use App\Listeners\Listeners\Admin\AddToUserStatistic;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        
        AddToStatistic::class => [
            AddToUserStatistic::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
