<?php

namespace App\Listeners\Listeners\Admin;

use App\Events\Events\AddToStatistic;
use App\Models\UserStatistic;

class AddToUserStatistic
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\Events\AddToStatistic $event
     * @return void
     */
    public function handle(AddToStatistic $event)
    {
        $statistic = UserStatistic::whereDate('date', now())->firstOrNew([
            'type' => $event->type,
            'user_id' => $event->user_id,
        ], [
            'date' => now(),
        ]);
        $statistic->val++;
        $statistic->save();
    }
}
