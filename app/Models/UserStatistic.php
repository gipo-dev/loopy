<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

class UserStatistic extends Model
{
    use AsSource;
    use Chartable;

    const TYPE_CORRECTION = 'correction',
        TYPE_WORD_REQUESTS = 'word_request';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $dates = ['date'];

    /**
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    /**
     * @var int[]
     */
    protected $attributes = [
        'val' => 0,
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $type
     */
    public function scopeType(Builder $builder, string $type)
    {
        $builder->where('type', $type);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
