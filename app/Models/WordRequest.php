<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WordRequest
 * @package App\Models
 *
 * @property \Illuminate\Support\Carbon $datetime
 * @property string $word
 * @property string $def
 * @property int $status_id
 */
class WordRequest extends Model
{
    /**
     * @var string
     */
//    protected $connection = 'remote';

    const STATUS_NEED_VERIFICATION = 1,
        STATUS_WAIT_FOR_PUBLISH = 8,
        STATUS_DENIED = 3,
        STATUS_PUBLISHED = 4;

    /**
     * @var string
     */
    protected $table = 'no_results';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $dates = ['time'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
