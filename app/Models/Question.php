<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * @var string
     */
    protected $connection = 'remote';

    public const STATUS_PUBLISHED = 1,
        STATUS_NEW = 2,
        STATUS_MODERATE = 3,
        STATUS_DELETION = 4;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = ['date'];
}
