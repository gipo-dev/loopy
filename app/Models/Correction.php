<?php

namespace App\Models;

use App\Casts\Serialize;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Correction extends Model
{
    public const STATUS_NEW = 0,
        STATUS_ACTIVE = 1,
        STATUS_DENIED = 2,
        STATUS_MODERATE = 3,
        STATUS_AUTODISABLE = 4,
        STATUS_WAIT_FOR_PUBLISH = 5;

    public const TYPE_EDIT = 0,
        TYPE_ADD = 1;

    /**
     * @var string
     */
    protected $connection = 'remote';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $dates = ['date'];

    /**
     * @var string[]
     */
    protected $casts = [
        'meanings' => Serialize::class,
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNew(Builder $builder)
    {
        return $builder->where('status', self::STATUS_NEW);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eword()
    {
        return $this->belongsTo(Word::class, 'word_id');
    }

    public function getWordAttribute()
    {
        if ($this->type == self::TYPE_ADD)
            return $this->getRawOriginal('word') ?? '';
        else
            return $this->eword->word ?? '';
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getTypeNameAttribute()
    {
        return __('admin.corrections.type.' . $this->type);
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getStatusNameAttribute()
    {
        return __('admin.corrections.status.' . $this->status);
    }
}
