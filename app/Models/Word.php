<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Word extends Model
{
    /**
     * @var string
     */
    protected $connection = 'remote';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = ['datetime'];

    /**
     * @var int[]
     */
    protected $attributes = [
        'dublicates' => 0,
        'meanings_count' => 0,
        'photos_count' => 0,
    ];

    /**
     * @return int
     */
    public function getDefAttribute()
    {
        return $this->attributes['meaning'];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meanings()
    {
        return $this->hasMany(Meaning::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meanings_new()
    {
        return $this->hasMany(Meaning::class)
            ->where('created', '>', now()->setDate(2022, 1, 1));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meanings_disabled()
    {
        return $this->hasMany(Meaning::class)
            ->where('created', '<', now()->setDate(2000, 2, 1))
            ->where('created', '>', now()->setDate(1999, 12, 1));
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $word
     */
    public function scopeSearch(Builder $builder, string $word)
    {
        $builder->where('word', 'like', $word);
    }

    /**
     * @param \App\Models\WordRequest $wordRequest
     * @return \App\Models\Word
     */
    public static function generate(WordRequest $wordRequest)
    {
        if (!Auth::user()->hasAccess('word-requests.trusted'))
            return;
        $word = Word::search($wordRequest->word)->first();
        if ($word) {
            $word->meanings_count++;
            $word->save();
        } else {
            $word = Word::create([
                'length' => mb_strlen($wordRequest->word),
                'first_letter' => mb_substr($wordRequest->word, 0, 1),
                'two_letters' => mb_substr($wordRequest->word, 0, 2),
                'slug' => Str::slug($wordRequest->word),
                'word' => $wordRequest->word,
                'meanings_count' => 1,
            ]);
        }
        if ($wordRequest->meaning) {
            if (!$word->meanings()->where('meaning', $wordRequest->meaning)->first())
                $word->meanings()->create([
                    'created' => now(),
                    'meaning' => $wordRequest->meaning,
                ]);
        }
        if ($wordRequest->id) {
            $wordRequest->update(['status_id' => WordRequest::STATUS_PUBLISHED]);
            WordRequest::on('remote')->where('id', $wordRequest->id)
                ->update(['status_id' => WordRequest::STATUS_PUBLISHED]);
        }

        return $word;
    }
}
