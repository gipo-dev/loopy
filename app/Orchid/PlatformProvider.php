<?php

namespace App\Orchid;

use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Access rights')),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles'),

            Menu::make('Запросы')
                ->href(route('admin.word-request.index'))
                ->icon('bubble')
//                ->route()
                ->permission('word-requests.edit')
                ->title('Модерирование'),

            Menu::make('Правки и добавления')
                ->href(route('admin.corrections.index'))
                ->icon('pencil'),

            Menu::make('Отзывы')
                ->href(route('admin.questions.index'))
                ->icon('like'),

            Menu::make('Статистика')
                ->href(route('platform.statistics'))
                ->icon('graph')
                ->permission('statistics.moderators')
                ->title('Супер'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
            ItemPermission::group('Запросы')
                ->addPermission('word-requests.edit', 'Просмотр и редактирование')
                ->addPermission('word-requests.trusted', 'Публиковать без проверки'),
            ItemPermission::group('Отзывы')
                ->addPermission('questions.edit', 'Просмотр и редактирование')
                ->addPermission('questions.trusted', 'Публиковать без проверки'),
            ItemPermission::group('Правки и добавления')
                ->addPermission('corrections.edit', 'Просмотр и редактирование')
                ->addPermission('corrections.trusted', 'Публиковать без проверки'),
            ItemPermission::group('Статистика')
                ->addPermission('statistics.moderators', 'Статистика модераторов'),
        ];
    }

    /**
     * @return string[]
     */
    public function registerSearchModels(): array
    {
        return [
            // ...Models
            // \App\Models\User::class
        ];
    }
}
