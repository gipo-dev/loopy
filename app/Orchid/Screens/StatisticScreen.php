<?php

namespace App\Orchid\Screens;

use App\Models\User;
use App\Models\UserStatistic;
use App\Orchid\Layouts\Admin\UserStatisticsCorrectionsChart;
use App\Orchid\Layouts\Admin\UserStatisticsWordRequestsChart;
use Orchid\Screen\Screen;

class StatisticScreen extends Screen
{
    /**
     * @var string
     */
    public $permission = 'statistics.moderators';

    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Статистика';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Статистика публикаций модераторов';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'statistics_corrections' => $this->getUserStatistics(UserStatistic::TYPE_CORRECTION),
            'statistics_word_requests' => $this->getUserStatistics(UserStatistic::TYPE_WORD_REQUESTS),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            UserStatisticsCorrectionsChart::class,
            UserStatisticsWordRequestsChart::class,
        ];
    }

    /**
     * @return array
     */
    private function getUserStatistics($type)
    {
        return User::get()->map(function (User $user) use ($type) {
            return $user->statistic()
                ->type($type)
                ->valuesByDays('val', now()->subDays(30), now(), 'date')
                ->toChart($user->name);
        });
    }
}
