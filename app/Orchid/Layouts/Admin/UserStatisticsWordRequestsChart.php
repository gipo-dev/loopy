<?php

namespace App\Orchid\Layouts\Admin;

use Orchid\Screen\Layouts\Chart;

class UserStatisticsWordRequestsChart extends Chart
{
    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Отмодерированые запросы';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'line';

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'statistics_word_requests';

    /**
     * @var int
     */
    protected $height = 300;

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = true;
}
