<?php

namespace App\Orchid\Layouts\Admin;

use Orchid\Screen\Layouts\Chart;

class UserStatisticsCorrectionsChart extends Chart
{
    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Отмодерированые правки/добавления';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'line';

    /**
     * @var int
     */
    protected $height = 300;

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'statistics_corrections';

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = true;
}
