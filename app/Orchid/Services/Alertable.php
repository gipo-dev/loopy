<?php


namespace App\Orchid\Services;


trait Alertable
{
    /**
     * @param $message
     */
    public function alertSuccess($message)
    {
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success', 'message' => $message]);
    }

    /**
     * @param $message
     */
    public function alertError($message)
    {
        $this->dispatchBrowserEvent('alert',
            ['type' => 'error', 'message' => $message]);
    }

    /**
     * @param $message
     */
    public function alertInfo($message)
    {
        $this->dispatchBrowserEvent('alert',
            ['type' => 'info', 'message' => $message]);
    }
}
