<?php

namespace App\Events;

use App\Models\WordRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WordRequestEditStarted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\Models\WordRequest
     */
    public $wordRequest;

    /**
     * @var int
     */
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(WordRequest $wordRequest, int $user_id)
    {
        $this->wordRequest = $wordRequest;
        $this->user_id = $user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('wordRequestEditors');
    }

    public function broadcastAs()
    {
        return 'editStarted';
    }
}
