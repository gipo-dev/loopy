<?php

namespace App\Events\Events\Admin;

use App\Models\WordRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WordRequestSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var WordRequest
     */
    public $model;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(WordRequest $wordRequest)
    {
        $this->model = $wordRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
