<?php

namespace App\Events;

use App\Models\Correction;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CorrectionEditStarted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\Models\Correction
     */
    public $correction;

    /**
     * @var int
     */
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Correction $correction, int $user_id)
    {
        $this->correction = $correction;
        $this->user_id = $user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('correctionEditors');
    }

    public function broadcastAs()
    {
        return 'editStarted';
    }
}
