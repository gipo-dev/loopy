<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CorrectionEditEnds implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $correctionId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($correctionId)
    {
        $this->correctionId = $correctionId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('correctionEditors');
    }

    public function broadcastAs()
    {
        return 'editEnds';
    }
}
