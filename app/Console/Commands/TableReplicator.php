<?php

namespace App\Console\Commands;

use App\Models\WordRequest;
use Illuminate\Console\Command;

class TableReplicator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:replicate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Реплицирует часть данных из боевой бд в рабочую';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pages = 20;
        WordRequest::whereNotIn('status_id', [WordRequest::STATUS_WAIT_FOR_PUBLISH, WordRequest::STATUS_NEED_VERIFICATION])->orWhereNull('status_id')->delete();
        WordRequest::on('remote')
            ->whereNull('status_id')
            ->latest('id')
            ->chunk(500, function ($words, $page) use (&$pages) {
                $this->info($page);
                foreach ($words as $word)
                    try {
                        WordRequest::insert($word->toArray());
                    } catch (\Exception $exception) {
                    }
                if ($page >= $pages)
                    return false;
            });
    }
}
