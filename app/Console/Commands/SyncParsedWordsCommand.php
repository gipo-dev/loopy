<?php

namespace App\Console\Commands;

use App\Models\Meaning;
use App\Models\Word;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class SyncParsedWordsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'words:parsed:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизирует новые слова и значения локальной бд с удаленной';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $words = Word::with('meanings_new')
            ->where(function ($q) {
                $q->where('dublicates', 255)
                    ->whereHas('meanings_new')->get();
            })->orWhere(function ($q) {
                $q->where('dublicates', '<', 200)
                    ->whereHas('meanings_new');
            })->get();
        $pb = $this->output->createProgressBar($words->count());
        $pb->setFormat('very_verbose');
        $pb->start();
        $words->each(function ($word) use (&$pb) {
            /** @var Word $word */
            $word_prod = Word::on('prod')
                ->firstOrCreate([
                    'word' => $word->word,
                ], Arr::except($word->getRawOriginal(), 'id'));
            foreach ($word->meanings_new as $meaning) {
                Meaning::on('prod')->create([
                    'word_id' => $word_prod->id,
                    'meaning' => $this->prepareMeaning($meaning->meaning),
                    'created' => now(),
                ]);
                $meaning->update(['created' => now()]);
            }
            $pb->advance();
        });
    }

    private function prepareMeaning($meaning)
    {
        return str_replace([
            "''"
        ], [
            '"'
        ], $meaning);
    }
}
