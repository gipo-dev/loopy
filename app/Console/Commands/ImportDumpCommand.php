<?php

namespace App\Console\Commands;

use App\Models\Word;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class ImportDumpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dump:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сравнивает и импортирует poncy.ru csv';

    private $filePath = 'article_all_1.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rows = $this->getRows();
        $pb = $this->output->createProgressBar(count($rows));
        $pb->start();
        foreach ($rows->chunk(100) as $chunk) {
            $exists_words = Word::with(['meanings'])->whereIn('word', $chunk->pluck('word'))->get()->keyBy(function ($el) {
                return mb_strtolower($el['word']);
            });
            foreach ($chunk as $item) {
                try {
                    $_word = $this->prepareWord($item['word']);
                    $word = $exists_words[$_word[0]] ?? $exists_words[$_word[1]] ?? false;
                    if (!$word) {
                        $slug = Str::slug($item['word']);
                        if (Word::where('slug', $slug)->count() > 0)
                            $slug .= '_' . now()->micro;
                        $word = Word::create([
                            'word' => $item['word'],
                            'slug' => $slug,
                            'length' => mb_strlen($item['word']),
                            'first_letter' => mb_substr($item['word'], 0, 1),
                            'two_letters' => mb_substr($item['word'], 0, 2),
                            'dublicates' => 255,
                        ]);
                    }
                    $meanings = $word->meanings->keyBy(function ($el) {
                        return mb_strtolower($el['meaning']);
                    });
                    foreach ($item['meanings'] as $meaning) {
                        if (!isset($meanings[mb_strtolower($meaning)])) {
                            if (mb_strlen($meaning) > 500)
                                continue;
                            $word->meanings()->create([
                                'created' => Carbon::parse('2022-12-22'),
                                'meaning' => $meaning,
                            ]);
                            $meanings[mb_strtolower($meaning)] = $meaning;
                        }
                    }
                    $pb->advance();
                } catch (\Exception $exception) {

                }
            }
        }
    }

    private function getRows()
    {
        $words = [];
        $row = 1;
        if (($handle = fopen(storage_path($this->filePath), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row == '2')
                    continue;
                $r = [
                    'word' => $data[0],
                    'meanings' => $this->array_iunique(explode("\r\n", $data[1])),
                ];
                $rows[] = $r;
            }
            fclose($handle);
        }
        return collect($rows);
    }

    private function array_iunique($array)
    {
        return array_intersect_key(
            $array,
            array_unique(array_map("mb_strtolower", $array))
        );
    }

    private function prepareWord($word)
    {
        return [mb_strtolower(str_replace('ё', 'е', $word)), mb_strtolower($word)];
    }
}
