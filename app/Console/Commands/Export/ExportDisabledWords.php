<?php

namespace App\Console\Commands\Export;

use App\Models\Word;
use Illuminate\Console\Command;

class ExportDisabledWords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:words:disabled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Экспортирует отключенные слова';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $cont = Word::has('meanings', '<', 1)->where('dublicates', '!=', 255)->update([
//            'dublicates' => 250,
//        ]);
//        dd($cont);
        $this->exportWords();
    }

    private function exportWords()
    {
        $text = '';
        $words = Word::with('meanings_new')
            ->where(function ($q) {
                $q->where('dublicates', 255)
                    ->whereHas('meanings_new')->get();
            })->orWhere(function ($q) {
                $q->where('dublicates', '<', 200)
                    ->whereHas('meanings_new');
            })->get();
//        $words = Word::with('meanings_new')
//            ->where('dublicates', '<', 200)
//            ->whereHas('meanings_new')->get();
        $rows = [];
        foreach ($words as $word) {
            $rows[] = [
                'key' => $word->word,
                'values' => $word->meanings_new->pluck('meaning')->toArray(),
                'db' => $word->except(['id'])->getOriginal(),
            ];
            dd($rows);
//            $text .= $word->word . PHP_EOL;
//            foreach ($word->meanings_new as $meaning)
//                $text .= '       ' . $meaning->meaning . PHP_EOL;
//            $text .= PHP_EOL;
        }
//        file_put_contents(storage_path('new_words_new_meanings.txt'), $text);
        file_put_contents(storage_path('export.json'), json_encode($rows));
    }
}
