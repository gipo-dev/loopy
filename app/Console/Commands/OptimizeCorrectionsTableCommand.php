<?php

namespace App\Console\Commands;

use App\Models\Correction;
use Illuminate\Console\Command;

class OptimizeCorrectionsTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corrections:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаляет корректировки, в которых нет значений';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $corrections = Correction::where('type', Correction::TYPE_EDIT)
            ->where('status', Correction::STATUS_NEW)
            ->get();
        $pb = $this->output->createProgressBar($corrections->count());
        $pb->start();
        foreach ($corrections as $correction) {
            try {
                if (!isset($correction->meanings[0])) {
                    $correction->update([
                        'status' => Correction::STATUS_AUTODISABLE,
                    ]);
                }
                $pb->advance();
            } catch (\Exception $exception) {
            }
        }
    }
}
