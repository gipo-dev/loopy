<?php

namespace App\Console\Commands;

use App\Models\WordRequest;
use Illuminate\Console\Command;

class OptimizeRequestsTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'requests:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Оптимизирует таблицу "no_results"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //id задачи 27210

        $by = 500;
        $total_count = round(WordRequest::whereNull('count')->count());
        $pb = $this->output->createProgressBar($total_count);
        $pb->setFormat('very_verbose');
        $pb->start();
        while (true) {
            $word_requests = WordRequest::whereNull('count')->limit($by)->get()
                ->unique(function ($el) {
                    return $el->word . '_' . $el->meaning;
                })->each(function ($request) use (&$pb) {
                    $word = str_replace('_', '\_', $request->word);
                    $meaning = str_replace('_', '\_', $request->meaning);
                    $total = WordRequest::where('word', 'like', $word)
                        ->where('meaning', $meaning)
                        ->where('id', '!=', $request->id)
                        ->get();
                    $request->update([
                        'count' => $total->count() + 1
                    ]);
                    try {
                        WordRequest::whereIn('id', $total->pluck('id'))
                            ->whereNull('count')
                            ->delete();
                    } catch (\Exception $exception) {
                        WordRequest::where('word', 'like', $word)
                            ->where('meaning', $meaning)
                            ->where('id', '!=', $request->id)
                            ->delete();
                    }
                    $pb->advance($total->count() + 1);
//                    if ($total->count() > 1)
//                        dd($request);
                });
            if ($word_requests->count() < 1)
                return 0;
        }
    }
}
