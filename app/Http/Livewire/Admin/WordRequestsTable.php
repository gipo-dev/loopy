<?php

namespace App\Http\Livewire\Admin;

use App\Events\Events\AddToStatistic;
use App\Events\WordRequestEditEnds;
use App\Events\WordRequestEditStarted;
use App\Models\Correction;
use App\Models\UserStatistic;
use App\Models\Word;
use App\Models\WordRequest;
use App\Orchid\Services\Alertable;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class WordRequestsTable extends Component
{
    use WithPagination;
    use Alertable;

    /**
     * @var string
     */
    protected $paginationTheme = 'bootstrap';

    /**
     * @var string[]
     */
    protected $listeners = [
        'echo:wordRequestEditors,.editStarted' => 'disableRow',
        'echo:wordRequestEditors,.editEnds' => 'enableRow',
        'updateRows' => 'updateRows',
    ];

    private const EDITABLE_FREE = [
        'id' => null,
        'type' => '',
        'text' => '',
    ];

    /**
     * @var array
     */
    public $editable_post = self::EDITABLE_FREE;

    /**
     * @var array
     */
    public $busy_posts = [];

    /**
     * @var array
     */
    public $sorting = [
        'row' => '',
        'desc' => true,
    ];

    /**
     * @var false[]
     */
    public $period = [
        'from' => false,
        'to' => false,
    ];

    /**
     * @var array
     */
    public $checked = [];

    /**
     * @var bool
     */
    public $checkAll = false;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.admin.word-requests-table', [
            'word_requests' => $this->updateRows(),
            'wait_for_publish' => $this->getWaitForPublishCount(),
        ]);
    }

    private function getQuery()
    {
        $query = Auth::user()->hasAccess('word-requests.trusted') ? WordRequest::with(['editor'])->whereNull('status_id')->orWhere('status_id', WordRequest::STATUS_NEED_VERIFICATION) : WordRequest::with([])->whereNull('status_id');
        if ($this->sorting['row'])
            $query->orderBy($this->sorting['row'], $this->sorting['desc'] ? 'desc' : 'asc');
        else {
            Auth::user()->hasAccess('word-requests.trusted') ? $query->orderByDesc('status_id')->orderBy('id', $this->sorting['desc'] ? 'desc' : 'asc') : $query->whereNull('status_id')->orderBy('id', $this->sorting['desc'] ? 'desc' : 'asc');
        }
        if ($this->period['from']) {
            $query->whereDate('time', '>=', Carbon::parse($this->period['from']))
                ->whereDate('time', '<=', Carbon::parse($this->period['to']));
        }
        return $query->paginate(30);
    }

    /**
     * @param \App\Models\WordRequest $wordRequest
     * @param string $type
     */
    public function edit(WordRequest $wordRequest, string $type)
    {
        $this->setEditable($wordRequest, $type);
    }

    /**
     * @param \App\Models\WordRequest $wordRequest
     * @param string $type
     */
    public function setEditable(WordRequest $wordRequest, string $type)
    {
        $this->setFree($this->editable_post['id']);
        $this->editable_post = $wordRequest->toArray();
        event(new WordRequestEditStarted($wordRequest, Auth::id()));
    }

    /**
     * @param $wordRequestId
     */
    public function setFree($wordRequestId)
    {
        event(new WordRequestEditEnds($wordRequestId));
    }

    /**
     * @param $data
     */
    public function disableRow($data)
    {
        if ($data['user_id'] != Auth::id()) {
            $this->busy_posts[$data['wordRequest']['id']] = $data['user_id'];
        }
    }

    /**
     * @param $data
     */
    public function enableRow($data)
    {
        unset($this->busy_posts[$data['wordRequestId']]);
    }

    public function updateRows()
    {
        $this->word_requests = $this->getQuery();
        return $this->getQuery();
    }

    /**
     * @param $row
     */
    public function sort($row)
    {
        $this->sorting = [
            'row' => $row,
            'desc' => !$this->sorting['desc'],
        ];
    }

    /**
     * @param $from
     * @param $to
     */
    public function setPeriod($from, $to)
    {
        $this->period = [
            'from' => Carbon::parse($from),
            'to' => Carbon::parse($to),
        ];
    }

    public function save()
    {
        if (Auth::user()->hasAccess('word-requests.trusted'))
            $this->editable_post['status_id'] = WordRequest::STATUS_WAIT_FOR_PUBLISH;
        else
            $this->editable_post['status_id'] = WordRequest::STATUS_NEED_VERIFICATION;

        $word = WordRequest::find($this->editable_post['id']);
        $word->update(
            array_merge(collect($this->editable_post)
                ->only(['word', 'meaning', 'status_id'])->toArray(), ['editor_id' => Auth::id()]),
        );
        WordRequest::on('remote')->where('id', $word->id)->update(['status_id' => $word->status_id]);
        $this->emit('modelSaved');
        $this->setFree($this->editable_post['id']);
        event(new AddToStatistic(UserStatistic::TYPE_WORD_REQUESTS, Auth::id()));
    }

    /**
     * @param \App\Models\WordRequest $wordRequest
     */
    public function delete($id)
    {
        WordRequest::whereIn('id', is_array($id) ? $id : [$id])->update([
            'status_id' => WordRequest::STATUS_DENIED,
            'editor_id' => Auth::id(),
        ]);
        WordRequest::on('remote')
            ->whereIn('id', is_array($id) ? $id : [$id])
            ->update(['status_id' => WordRequest::STATUS_DENIED]);
        $this->emit('modelSaved');
        $this->setFree($this->editable_post['id']);
        event(new AddToStatistic(UserStatistic::TYPE_WORD_REQUESTS, Auth::id()));
    }

    /**
     * @return int
     */
    public function getWaitForPublishCount()
    {
        if (Auth::user()->hasAccess('corrections.trusted'))
            return WordRequest::where('status_id', WordRequest::STATUS_WAIT_FOR_PUBLISH)->count();
        return WordRequest::where('status_id', WordRequest::STATUS_NEED_VERIFICATION)->where('editor_id', Auth::id())->count();
    }

    public function publish()
    {
        $word_requests = WordRequest::where('status_id', WordRequest::STATUS_WAIT_FOR_PUBLISH)->get();
        if ($word_requests->count() > 0) {
            $word_requests->each(function (WordRequest $word_request) {
                Word::generate($word_request);
            });
            $this->alertSuccess("Опубликовано {$word_requests->count()} определений");
            (new Client())->get('https://loopy.ru/need_reindex.php?key=loopy_admin');
        } else
            $this->alertError('Нет новых определений');
    }
}
