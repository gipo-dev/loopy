<?php


namespace App\Http\Livewire\Admin;

use App\Models\Correction;
use Illuminate\Support\Facades\Auth;

trait Busyable
{
    public $editable_post = self::EDITABLE_FREE;

    public $busy_posts = [];

    /**
     * @param \App\Models\Correction $correction
     * @param string $type
     */
    public function edit(Correction $correction, string $type)
    {
        $this->setEditable($correction, $type);
    }

    /**
     * @param \App\Models\Correction $correction
     * @param string $type
     */
    public function setEditable(Correction $correction, string $type)
    {
        $this->setFree($this->editable_post['id']);
        $this->editable_post = $correction->toArray();
        try {
            event(new $this->onEditEvent($correction, Auth::id()));
        } catch (\Exception $exception) {
        }
    }

    /**
     * @param $correctionId
     */
    public function setFree($correctionId)
    {
        event(new $this->onEditEndsEvent($correctionId));
    }

    /**
     * @param $data
     */
    public function disableRow($data)
    {
        if ($data['user_id'] != Auth::id()) {
            $this->busy_posts[$data['correction']['id']] = $data['user_id'];
        }
    }

    /**
     * @param $data
     */
    public function enableRow($data)
    {
        unset($this->busy_posts[$data['correctionId']]);
    }
}
