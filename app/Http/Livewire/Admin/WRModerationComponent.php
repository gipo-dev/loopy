<?php

namespace App\Http\Livewire\Admin;

use App\Models\Meaning;
use App\Models\Word;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class WRModerationComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.admin.w-r-moderation-component', [
            'words' => Word::with(['meanings_new'])
                ->where('dublicates', 255)
                ->orWhere(function ($q) {
                    $q->whereHas('meanings_new')
                        ->where('dublicates', 255);
                })
                ->orderBy('word')
                ->paginate(300),
        ]);
    }

    /**
     * @param \App\Models\Word $word
     */
    public function deleteWord(Word $word)
    {
        $word->update(['dublicates' => 250]);
    }

    /**
     * @param \App\Models\Meaning $meaning
     */
    public function deleteDef(Meaning $meaning)
    {
        $meaning->update(['created' => Carbon::parse('2000-1-1')]);
    }
}
