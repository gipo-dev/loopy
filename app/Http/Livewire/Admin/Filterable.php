<?php


namespace App\Http\Livewire\Admin;


use Carbon\Carbon;

trait Filterable
{
    /**
     * @var array
     */
    public $sorting = [
        'row' => '',
        'desc' => true,
    ];

    /**
     * @var false[]
     */
    public $period = [
        'from' => false,
        'to' => false,
    ];

    /**
     * @param $row
     */
    public function sort($row)
    {
        $this->sorting = [
            'row' => $row,
            'desc' => !$this->sorting['desc'],
        ];
    }

    /**
     * @param $from
     * @param $to
     */
    public function setPeriod($from, $to)
    {
        $this->period = [
            'from' => Carbon::parse($from),
            'to' => Carbon::parse($to),
        ];
    }
}
