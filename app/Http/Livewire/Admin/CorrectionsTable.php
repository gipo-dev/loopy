<?php

namespace App\Http\Livewire\Admin;

use App\Events\CorrectionEditEnds;
use App\Events\CorrectionEditStarted;
use App\Events\Events\AddToStatistic;
use App\Models\Correction;
use App\Models\Meaning;
use App\Models\UserStatistic;
use App\Models\Word;
use App\Models\WordRequest;
use App\Orchid\Services\Alertable;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class CorrectionsTable extends Component
{
    use WithPagination;
    use Filterable;
    use Busyable;
    use Alertable;

    /**
     * @var string
     */
    protected $paginationTheme = 'bootstrap';

    protected $onEditEvent = correctionEditStarted::class;
    protected $onEditEndsEvent = correctionEditEnds::class;

    /**
     * @var string[]
     */
    protected $listeners = [
        'echo:correctionEditors,.editStarted' => 'disableRow',
        'echo:correctionEditors,.editEnds' => 'enableRow',
    ];

    private const EDITABLE_FREE = [
        'id' => null,
        'type' => '',
        'text' => '',
    ];

    /**
     * @var array
     */
    public $checked = [];

    /**
     * @var bool
     */
    public $checkAll = false;

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var string
     */
    public $search_field = 'email';

    /**
     * @var false[]
     */
    public $period = [
        'from' => false,
        'to' => false,
    ];

    public function mount()
    {
        $this->search = request()->word ?? '';
        $this->search_field = request()->field ?? 'email';
        if (request()->period_from ?? false)
            $this->period['from'] = Carbon::parse(request()->period_from);
        if (request()->period_to ?? false)
            $this->period['to'] = Carbon::parse(request()->period_to);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.admin.corrections-table', [
            'corrections' => $this->getQuery()->paginate(30),
            'wait_for_publish' => $this->getWaitForPublishCount(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function getQuery()
    {
        $query = Auth::user()->hasAccess('corrections.trusted') ? Correction::with(['editor', 'eword'])->whereIn('status', [Correction::STATUS_NEW, Correction::STATUS_MODERATE]) : Correction::with(['eword'])->new();
        if ($this->sorting['row'])
            $query->orderBy($this->sorting['row'], $this->sorting['desc'] ? 'desc' : 'asc');
        else {
            if (Auth::user()->hasAccess('corrections.trusted'))
                $query->orderBy('id', $this->sorting['desc'] ? 'desc' : 'asc');
            else
                $query->orderBy('id', $this->sorting['desc'] ? 'desc' : 'asc');
        }
        if ($this->period['from']) {
            $query->whereDate('date', '>=', Carbon::parse($this->period['from']))
                ->whereDate('date', '<=', Carbon::parse($this->period['to']));
        }
        if ($this->search != '' && $this->search_field != '') {
            $query->where($this->search_field, 'like', $this->search . '%');
        }
        return $query;
    }

    /**
     * @param array|int $id
     */
    public function delete($id)
    {
        Correction::whereIn('id', is_array($id) ? $id : [$id])->update([
            'status' => Correction::STATUS_DENIED,
            'editor_id' => Auth::id(),
        ]);
        $this->emit('modelSaved');
        $this->setFree($this->editable_post['id']);
        event(new AddToStatistic(UserStatistic::TYPE_CORRECTION, Auth::id()));
    }

    public function save()
    {
        $correction = Correction::find($this->editable_post['id']);
        $correction->fill(Arr::except($this->editable_post, ['eword']));
        if (Auth::user()->hasAccess('corrections.trusted')) {
            $correction->status = Correction::STATUS_WAIT_FOR_PUBLISH;
            $correction->editor_id = $correction->editor_id ?? Auth::id();
        } else {
            $correction->status = Correction::STATUS_MODERATE;
            $correction->editor_id = Auth::id();
        }
        $correction->save();
        $this->emit('modelSaved');
        $this->setFree($this->editable_post['id']);
        event(new AddToStatistic(UserStatistic::TYPE_CORRECTION, Auth::id()));
    }

    public function publish()
    {
        $corrections = Correction::where('status', Correction::STATUS_WAIT_FOR_PUBLISH)->get();
        if ($corrections->count() > 0) {
            $corrections->each(function ($correction) {
                $word = Word::generate(new WordRequest([
                    'word' => $correction->word,
                    'meaning' => null,
                ]));
                if ($correction->type == Correction::TYPE_ADD) {
                    foreach ($correction->meanings as $id => $meaning) {
                        if (!$word->meanings()->where('meaning', $meaning)->first()) {
                            $word->meanings()->create([
                                'created' => now(),
                                'meaning' => $meaning,
                            ]);
                        }
                    }
                } else if ($correction->type == Correction::TYPE_EDIT) {
                    foreach ($correction->meanings as $id => $meaning) {
                        if ($id != 0) {
                            Meaning::where('id', $id)->update([
                                'meaning' => $meaning,
                            ]);
                        } else {
                            if (!$word->meanings()->where('meaning', $meaning)->first()) {
                                $word->meanings()->create([
                                    'created' => now(),
                                    'meaning' => $meaning,
                                ]);
                            }
                        }
                    }
                }
                $correction->update(['status' => Correction::STATUS_ACTIVE]);
            });
            (new Client())->get('https://loopy.ru/need_reindex.php?key=loopy_admin');
            $this->alertSuccess("Опубликовано {$corrections->count()} корректировок");
        } else
            $this->alertError('Нет новых корректировок');
    }

    public function deleteChecked()
    {
        $this->delete($this->checked);
        $this->checked = [];
    }

    public function updatedCheckAll($value)
    {
        if ($value)
            $this->checked = $this->getQuery()->paginate(30)->pluck('id')->toArray();
        else
            $this->checked = [];
    }

    private function getWaitForPublishCount()
    {
        if (Auth::user()->hasAccess('corrections.trusted'))
            return Correction::where('status', Correction::STATUS_WAIT_FOR_PUBLISH)->count();
        return Correction::where('status', Correction::STATUS_MODERATE)->where('editor_id', Auth::id())->count();
    }
}
