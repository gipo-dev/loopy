<?php

namespace App\Http\Livewire\Admin;

use App\Events\WordRequestEditEnds;
use App\Events\WordRequestEditStarted;
use App\Models\Question;
use App\Models\Word;
use App\Models\WordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\Request;
use Livewire\WithPagination;

class QuestionsTable extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.admin.questions-table', [
            'questions' => Auth::user()->hasAccess('questions.trusted') ? Question::whereIn('status', [Question::STATUS_NEW, Question::STATUS_MODERATE, Question::STATUS_DELETION])->orderByDesc('status')->latest('id')->paginate(30) : Question::where('status', Question::STATUS_NEW)->latest('id')->paginate(30),
        ]);
    }

    /**
     * @param \App\Models\Question $question
     */
    public function publish(Question $question)
    {
        if (Auth::user()->hasAccess('questions.trusted'))
            $question->status = Question::STATUS_PUBLISHED;
        else
            $question->status = Question::STATUS_MODERATE;
        $question->save();
    }

    /**
     * @param \App\Models\Question $question
     */
    public function delete(Question $question)
    {
        if (Auth::user()->hasAccess('questions.trusted'))
            $question->delete();
        else {
            $question->status = Question::STATUS_DELETION;
            $question->save();
        }
    }
}
